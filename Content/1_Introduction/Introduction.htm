﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
        <link href="../Resources/TableStyles/SimpleDefinitions.css" rel="stylesheet" MadCap:stylesheetType="table" />
    </head>
    <body>
        <h1 style="page-break-before: avoid;" MadCap:autonum="1. &#160;">Introduction</h1>
        <p style="text-align: left;">S3 Connector provides an AWS S3- and IAM-compatible interface to Scality RING, with support for core AWS S3 bucket and object APIs, including multipart upload (MPU). Scale-out capability enables concurrent same-bucket access from multiple S3 Connector instances for both read and write operations. </p>
        <p style="text-align: left;">S3 Connector scales seamlessly from a distributed system atop a minimum cluster of three standard x86 servers to systems comprising thousands of physical storage servers with a total storage capacity that runs into hundreds of petabytes.</p>
        <p style="text-align: left;">In addition,  <MadCap:variable name="General.ProductNameShort" /> supports the AWS IAM (Identity and Access Management) model of accounts, users, groups and policies, and integration is available for Microsoft Active Directory through ADFS and for Single Sign-On (SSO) services through the SAML 2.0 protocol.</p>
        <p style="text-align: left;">S3 Connector uses Ansible as a federated deployment manager, and is installed using pre-built Docker images. A single Ansible command can deploy   separate Docker images  for:</p>
        <ul>
            <li style="text-align: left;">S3 Connector
            </li>
            <li style="text-align: left;"> S3 Metadata components</li>
            <li style="text-align: left;">S3 Vault authentication components</li>
            <li style="text-align: left;"><span class="ProprietaryNonConventional">sproxyd</span> components for connecting to back-end  storage</li>
            <li style="text-align: left;">Nginx server for SSL termination and internal failover</li>
            <li style="text-align: left;">Logging components that install Elasticsearch, Logstash, and Kibana (ELK) </li>
        </ul>
        <p style="text-align: left;" MadCap:conditions="PrintGuides.Not Ready Yet">
            <MadCap:conditionalText MadCap:conditions="PrintGuides.S3">Because Scality RING requires a minimum of three servers, the </MadCap:conditionalText>S3 Connector containers are installed and deployed on a minimum of three servers in the cluster to ensure proper operation and resiliency of the S3 Metadata service, which functions as a distributed consensus cluster of three servers for managing bucket metadata.</p>
        <p style="text-align: left;" MadCap:conditions="PrintGuides.Not Ready Yet">S3 Connector containers can be deployed and active on more
than five servers, to provide a scalable number of S3 endpoints for applications with bandwidth-intensive
workloads. Regardless of the number of machines on which S3 Connector is deployed, the
S3 Metadata service runs on <span class="inline-comment">at least three </span>servers for high availability.</p>
        <p style="text-align: left;">S3 Connector uses Nginx for internal component-level failover only. It is not intended as an external traffic load balancer. A dedicated external load balancer is recommended for best overall throughput and optimal failover across S3 Connector machines and endpoints.</p>
        <p style="text-align: left;">For security, every effort has been made to alleviate the need for elevated privileges. Sudo privileges are only needed to install software dependencies and remove incompatible software from the target servers. </p>
        <p style="text-align: left;" MadCap:conditions="PrintGuides.S3">All components, including S3 Connector can be run on industry-standard servers. As a software storage back end, S3 Connector performs at an extremely high level, becoming more reliable as the system grows, unlike traditional storage appliances and systems. </p>
        <p>S3 Connector CRR provides disaster recovery capabilities in the event of a single-site failure or network outage.</p>
        <h2 MadCap:autonum="1.1. &#160;">About Docker Images</h2>
        <p style="text-align: left;">Docker images are Docker’s build component. Using Docker images ensures that installed applications always run the same, regardless of the run environment. </p>
        <p style="text-align: left;">S3 Connector components are installed in pre-built Docker images that Ansible can deploy to the target environment from the <MadCap:variable name="General.CompanyName" /> Docker Hub website (<a href="https://hub.docker.com/u/scality/" id="Hyperlink">https://hub.docker.com/u/scality/</a>). </p>
        <p class="BoxNote" MadCap:conditions="PrintGuides.Not Ready Yet">Online <MadCap:variable name="General.ProductName"></MadCap:variable> component installation requires that each server in the target environment be logged into the Docker Hub.</p>
        <p style="text-align: left;">Pre-built images for the S3 Connector target environment include:</p>
        <table style="mc-table-style: url('../Resources/TableStyles/SimpleDefinitions.css');margin-left: auto;margin-right: auto;" class="TableStyle-SimpleDefinitions" cellspacing="0">
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 100px;" />
            <col class="TableStyle-SimpleDefinitions-Column-Column1" style="width: 476px;" />
            <tbody>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">scality/s3</td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="text-align: left;">An open-source Node.js implementation of a server handling the Amazon S3 protocol</td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">scality/metadata</td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="text-align: left;">The Metadata module facilitates the storage, replication and durability of metadata for all services (bucket, IAM, etc) that require a persistent metadata model, generating and manipulating metadata in key-indexed key/value tables.</td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">scality/vault</td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="text-align: left;">Vault manages Identity and Access Management (IAM)- and Security Token Service (STS)-related functions for <MadCap:variable name="General.ProductNameShort" />. Vault, as AWS IAM, manages accounts, users, access keys, secret keys, groups, roles, and other entities, which Vault serves as a distributed and replicated database. For STS, Vault allows users to assume roles through accounts. Vault also verifies the authenticity of every request that reaches a S3 connector.</td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">scality/elk</td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="text-align: left;">Elasticsearch, Logstash, and Kibana stack. Elasticsearch is a search and analytics engine. Logstash is a server&#8209;side data processing pipeline that ingests data from multiple sources simultaneously, transforms it, and then sends it to a “stash” like Elasticsearch. Kibana lets users visualize data with charts and graphs.</td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyE-Column1-Row1">scality/sproxyd</td>
                    <td class="TableStyle-SimpleDefinitions-BodyD-Column1-Row1" style="text-align: left;">The sproxyd connector provides a flexible REST-based API for interacting with storage through simple GET, PUT, and DELETE commands. It functions as a fastCGI&#160;interface, so it can be deployed on the application server, storage nodes, or a dedicated server functioning as a proxy for S3 Connector or other <MadCap:variable name="General.CompanyName" /> connectors.</td>
                </tr>
                <tr class="TableStyle-SimpleDefinitions-Body-Row1">
                    <td class="TableStyle-SimpleDefinitions-BodyB-Column1-Row1">redis:3.2-alpine</td>
                    <td class="TableStyle-SimpleDefinitions-BodyA-Column1-Row1" style="text-align: left;">A Redis container, used for:
<ul><li style="text-align: left;">Policy caching for Vault–SAML session cookie caching to enable single sign-on</li><li style="text-align: left;">UTAPI (Utilization API) health metric storage for load balancing</li><li style="text-align: left;">Utilization metrics (useful for billing)</li><li style="text-align: left;">Raft leader change notification for the Metadata service</li></ul></td>
                </tr>
            </tbody>
        </table>
    </body>
</html>