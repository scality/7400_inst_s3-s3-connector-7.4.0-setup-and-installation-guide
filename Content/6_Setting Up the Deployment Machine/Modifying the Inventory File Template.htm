﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1 MadCap:autonum="1. &#160;"><a name="top"></a>Modifying Ansible Inventory Files</h1>
        <p>The Ansible <span class="ProprietaryNonConventional">Inventory</span> file describes the S3 Connector deployment configuration, including specifying the target servers used for the deployment and indicating where the various component services are to be hosted. Ansible orchestrates the federated deployment of services across all target servers according to the inventory file. </p>
        <div>
            <p>A set of default standard inventory file templates is available to be edited for specific requirements, depending on the number of data centers (sites) in the deployment: </p>
            <ul>
                <li>Single-site</li>
                <li>Two-site stretched</li>
                <li>Three-site stretched</li>
            </ul>
        </div>
        <p style="text-align: left;">To declare the machines to which S3 Connector components are to be deployed, use the appropriate inventory file previously copied to the deployment server under <em>env/{{targetEnvironmentName}}</em>. </p>
        <h2 MadCap:autonum="1.1. &#160;">Defining Stateless and Stateful Groups in Multi-Site Decoupled Deployments</h2>
        <p>For disaster recovery procedures to function correctly, stateless and stateful hosts must be established in distinct groups for the various sites during installation. The following code sample shows the configuration for a two-site stretched architecture.</p><pre xml:space="preserve">[wsb_site1]

[site_1]
	stateful_site1_1
	stateful_site1_2
	stateful_site1_3

[wsb_site2]
	stateful_site2_3

[site_2]
	stateful_site2_1
	stateful_site2_2

[stateless_site1]
	stateless_site1_1
	stateless_site1_2
	stateless_site1_3

[stateless_site2]
	stateless_site2_1
	stateless_site2_2
	stateless_site2_3

[stateless:children]
	stateless_site1
	stateless_site2</pre>
        <h2 MadCap:autonum="1.2. &#160;">Installation on a Single Site</h2>
        <p style="text-align: left;">A minimum of three servers is required for an <MadCap:variable name="General.ProductName" /> installation on a single-site RING. The S3 Metadata service  establishes a special cluster with a quorum that works most efficiently with an odd number of servers.</p>
        <p style="text-align: left;">Vault stores its account information in the installed S3 Metadata components, so each installed Vault instance is able to access account information created by a companion Vault component.</p>
        <p class="BoxNote" MadCap:conditions="PrintGuides.Not Ready Yet"><span class="ProperNameBold"><a href="https://github.com/scality/Federation/blob/rel/6.4/env/client-template/inventory.1site">client-template/inventory.1site</a></span> is the only inventory file template to use in the installation of <MadCap:variable name="General.ProductName" /> on a single-site RING.</p>
        <h2 MadCap:autonum="1.3. &#160;"><a name="Installa"></a>Installation on Stretched RINGs for Two or Three Sites</h2>
        <p style="text-align: left;">The <MadCap:variable name="General.ProductName" /> supports deployment across either two or three data centers (sites) using a stretched model. The  stretched deployment model is designed to continue service and data availability even in the event of a data center outage, and to protect the data in the event of a site outage. Some of these availability properties, data protection schemes, storage overhead, and failover mechanisms differ between 2-site and 3-site deployments. For information on administrator actions pertaining to site failover, see the <span class="PublicationName"><MadCap:variable name="General.Operations Guide" /></span>.</p>
        <p style="text-align: left;">The stretched model uses a single data RING, composed of machines distributed and accessed across the sites. Data is protected using both a distributed erasure coding and replication scheme according to best practices for 2-site and 3-site data durability requirements.</p>
        <p style="text-align: left;">In these stretched deployments, the S3 Metadata service is also distributed across servers on the two sites. Because the Metadata service is run as a special cluster of five servers, the metadata distribution scheme is non-uniform across the sites, as described below. The 2-site and 3-site inventory template files described above can serve as the basis of deploying these stretched modes, but will require some editing and customization for specific site requirements. Please consult a Scality Customer Support Engineer (CSE) for further details and assistance in editing the template files. </p>
        <p class="BoxNote">The stretched deployment must have a high-speed, low-latency (&lt; 10 ms) network connection between the sites. See <MadCap:xref href="../5_Multi-Site Deployments/Multi-Site Deployments.htm">“Multi-Site Deployments” on page&#160;1</MadCap:xref>.</p>
        <p style="text-align: left;">With five servers across the two or three sites, the Metadata service has the resilience to automatically recover and maintain availability from one or two simultaneous server failures. The service automatically reassigns a Metadata server and the Metadata leader during nominal and failover operations and maintains consistency across all active servers. The stretched configurations also ensure that the service can be continued either automatically or through administrator initiated recovery procedures after a site failure, as described fully in the <span class="PublicationName"><MadCap:variable name="General.Operations Guide" /></span>.</p>
        <h2 MadCap:autonum="1.4. &#160;">Template Changes to Enable ELK</h2>
        <p style="page-break-after: avoid;">The ELK stack is disabled by default.  To enable the ELK stack on <MadCap:variable name="General.ProductName" />, uncomment the sites under <code>[loggers:children]</code> (the exact file depending on the number of sites).</p>
        <ul>
            <li>
                <p style="page-break-after: avoid;">If the deployment used the <span class="ProprietaryFileName">env/{{targetEnvironmentName}}/inventory.1site</span> file, uncomment the line <code>#stateful</code> in the file.</p><pre style="page-break-inside: avoid;">[loggers:children]

# Define here the ELK server groups

 stateful</pre>
            </li>
            <li>
                <p MadCap:conditions="" style="page-break-after: avoid;">If the deployment used the <span class="ProprietaryFileName">env/{{targetEnvironmentName}}/inventory.2site</span> file, uncomment the line <code>#active</code> in the file.</p><pre style="page-break-inside: avoid;" xml:space="preserve">[loggers:children]

active</pre>
            </li>
            <li>
                <p MadCap:conditions="" style="page-break-after: avoid;">If the deployment used the <span class="ProprietaryFileName">env/{{targetEnvironmentName}}/inventory.3site</span> file, uncomment the line <code>#all_sites</code> in the file.</p><pre style="page-break-inside: avoid;" xml:space="preserve">[loggers:children]

all_sites</pre>
            </li>
        </ul>
        <p class="BoxNote">To enable forwarding of filtered logs see <MadCap:xref href="Modifying the Group Variables Template.htm#top">“Modifying the group_vars/all File” on page&#160;1</MadCap:xref>.</p>
        <h2 MadCap:conditions="" MadCap:autonum="1.5. &#160;">Template Changes for Kibana and COSBench</h2>
        <p MadCap:conditions="" style="text-align: left;">To use Kibana for analysis and visualization of the log files, assign port 5601 on the logger machines in the Kibana setup. </p>
        <p MadCap:conditions="" style="page-break-after: avoid;text-align: left;">Optionally, to measure cloud storage performance, install the COSBench controller on a separate logger machine by uncommenting the <code>cosbench_controller</code> section in the template file (making sure to replace “example.com” with the DNS or IP address of the logger machine). </p><pre MadCap:conditions="">#[cosbench_controller]
#logger.example.com</pre>
        <p MadCap:conditions="" style="text-align: left;">Next, open the controller Web UI at <span class="HTMLCode" style="font-style: italic;">http://logger.example.com:19088/controller/index.html</span>.</p>
        <p MadCap:conditions="" style="page-break-after: avoid;text-align: left;">To set up endpoints for the COSBench workload, uncomment the <code style="font-style: normal;">cosbench_drivers</code> section in the template file and replace the examples with the server DNS names or IP addresses:</p><pre MadCap:conditions="" xml:space="preserve">#[cosbench_drivers]
#server1.example.com
#server2.example.com
#server3.example.com
#server4.example.com
#server5.example.com</pre>
        <p MadCap:conditions="" style="text-align: left;"> By placing cosbench drivers on the same machines as the S3 runners,  localhost can serve as the endpoint for the cosbench workload (instead of mapped DNS names or IP addresses). This is possible because Ansible links the <code>S3 runners</code> section with the <code>cosbench_drivers</code> section of the inventory file.</p>
    </body>
</html>